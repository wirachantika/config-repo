package id.co.sigma.day4.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import id.co.sigma.day4.model.Account;
import id.co.sigma.day4.model.Users;
import id.co.sigma.day4.repository.NasabahRepository;

@Service
@CacheConfig(cacheNames = "nasabahService")
public class NasabahRegistrationService {
	@Autowired
	private NasabahRepository nasabahRepository;

	@Cacheable(value= "NasabahRegistration.Nasabah.findAllNasabah", unless = "#result==null")
	public List<Users> findAll() {
		return nasabahRepository.findAll();
	}

	@Cacheable(value= "NasabahRegistration.Nasabah.findNasabahById", unless = "#result==null")
	public Users findByID(Long id) {
		return nasabahRepository.findById(id).get();
	}

	@Caching(evict = {
			@CacheEvict(value = "NasabahRegistration.Nasabah.findAllNasabah", allEntries=true, beforeInvocation=true),
			@CacheEvict(value = "NasabahRegistration.Nasabah.findNasabahById", allEntries=true, beforeInvocation=true)
	})
	public void save(Users nasabah) {
		nasabahRepository.save(nasabah);
	}

	@Caching(evict = {
			@CacheEvict(value = "minicore.account.findAllAccount", allEntries = true, beforeInvocation = true),
			@CacheEvict(value = "minicore.Account.findAccountById", allEntries = true, beforeInvocation = true) })
	public void insert(Users nasabah) {
		nasabahRepository.save(nasabah);
	}
	
	@Caching(evict = {
			@CacheEvict(value = "minicore.account.findAllAccount", allEntries = true, beforeInvocation = true),
			@CacheEvict(value = "minicore.Account.findAccountById", allEntries = true, beforeInvocation = true) })
	public void update(Users nasabah) {
		nasabahRepository.save(nasabah);
	}
	
	@Caching(evict = {
			@CacheEvict(value = "NasabahRegistration.Nasabah.findAllNasabah", allEntries=true, beforeInvocation=true),
			@CacheEvict(value = "NasabahRegistration.Nasabah.findNasabahById", allEntries=true, beforeInvocation=true)
	})
	public void deleteByID(Long id) {
		Users nasabah = nasabahRepository.findById(id).get();
		nasabahRepository.delete(nasabah);
	}

}