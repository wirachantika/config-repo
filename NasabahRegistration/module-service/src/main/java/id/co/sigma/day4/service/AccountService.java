package id.co.sigma.day4.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import id.co.sigma.day4.model.Account;
import id.co.sigma.day4.repository.RekeningRepository;

@Service("accountService")
@CacheConfig(cacheNames = "accountService")
public class AccountService {
	@Autowired
	private RekeningRepository repository;
	
	@Cacheable(value = "minicore.account.findAllAccount", unless = "#result==null")
	public List<Account> findAllAccount(){
		return repository.findAll();
	}
	
	@Cacheable(value = "minicore.account.findAccountById", unless = "#result==null")
	public Account findById(Long id){
		return repository.findById(id).get();
	}
	
	@Caching(evict = {
			@CacheEvict(value = "minicore.account.findAllAccount", allEntries = true, beforeInvocation = true),
			@CacheEvict(value = "minicore.Account.findAccountById", allEntries = true, beforeInvocation = true) })
	public void insert(Account account) {
		repository.save(account);
	}
	
	@Caching(evict = {
			@CacheEvict(value = "minicore.account.findAllAccount", allEntries = true, beforeInvocation = true),
			@CacheEvict(value = "minicore.Account.findAccountById", allEntries = true, beforeInvocation = true) })
	public void update(Account account) {
		repository.save(account);
	}
	
	@Caching(evict = {
			@CacheEvict(value = "minicore.account.findAllAccount", allEntries = true, beforeInvocation = true),
			@CacheEvict(value = "minicore.Account.findAccountById", allEntries = true, beforeInvocation = true) })
	public void deleteByID(Long id) {
		Account account = repository.findById(id).get();
		repository.delete(account);
	}
	
	@Cacheable(value = "minicore.account.findAccountById", unless = "#result==null")
	public Account findByAccountNumber(String accountNumber) {
		return repository.findByNoRekening(accountNumber);
	}
}
