package id.co.sigma.day4.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "account")
public class Account implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//@Column(name = "id_rekening")
	private Long id;
	
	@Column(name = "noRekening", nullable = false, unique = true)
	private String noRekening;
	
	@Column(nullable = false, unique = false)
	private BigDecimal saldo;
	
	@JsonIgnore
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_nasabah")
	private Users users;
	
	public Account() {
		super();
	}

	public Account(Long id, String no_rekening, BigDecimal saldo, Users users) {
		this.id= id;
		this.noRekening = no_rekening;
		this.saldo = saldo;
		this.users = users;
	}

	public Long getId() {
		return id;
	}

	public void setId_rekening(Long id) {
		this.id = id;
	}

	public String getNo_rekening() {
		return noRekening;
	}

	public void setNo_rekening(String no_rekening) {
		this.noRekening = no_rekening;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	public Users getUser() {
		return users;
	}

	public void setNasabah(Users users) {
		this.users = users;
	}
	
}
