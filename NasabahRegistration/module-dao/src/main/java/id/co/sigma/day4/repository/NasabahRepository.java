package id.co.sigma.day4.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.sigma.day4.model.Users;

public interface NasabahRepository extends JpaRepository<Users, Long>{
	
}
