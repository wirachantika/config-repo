package id.co.sigma.day4.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.sigma.day4.model.Account;

public interface RekeningRepository extends JpaRepository<Account, Long>{
	
	public Account findByNoRekening(String noRekening);
}
