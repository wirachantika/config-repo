package co.id.sigma.day4.request;


public class InsertUserRequest {

	private Long id_nasabah;
	private String userName;
	private String no_hp;
	private String alamat;
	private String accountNumber;
	public InsertUserRequest(Long id_nasabah, String userName, String no_hp, String alamat, String accountNumber) {
		super();
		this.id_nasabah = id_nasabah;
		this.userName = userName;
		this.no_hp = no_hp;
		this.alamat = alamat;
		this.accountNumber = accountNumber;
	}
	public Long getId_nasabah() {
		return id_nasabah;
	}
	public void setId_nasabah(Long id_nasabah) {
		this.id_nasabah = id_nasabah;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getNo_hp() {
		return no_hp;
	}
	public void setNo_hp(String no_hp) {
		this.no_hp = no_hp;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	

	
}
