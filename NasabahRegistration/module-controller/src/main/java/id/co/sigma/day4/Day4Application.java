package id.co.sigma.day4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@SpringBootApplication
@EnableConfigurationProperties
@ConfigurationProperties
@EnableEurekaClient
@EnableCaching
@EnableAutoConfiguration
@Configuration
@EnableDiscoveryClient
public class Day4Application {

	public static void main(String[] args) {
		SpringApplication.run(Day4Application.class, args);
		
//		String pass = "12345";
//		System.out.println(new BCryptPasswordEncoder().encode(pass));
	}

}
