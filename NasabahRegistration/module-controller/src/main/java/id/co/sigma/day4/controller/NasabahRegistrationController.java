package id.co.sigma.day4.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import co.id.sigma.day4.request.InsertUserRequest;
import co.id.sigma.day4.request.RequestBalance;
import co.id.sigma.day4.request.RequestDeposit;
import co.id.sigma.day4.request.RequestWithdrawl;
import co.id.sigma.day4.request.UpdateUserRequest;
import co.id.sigma.day4.response.BaseResponse;
import co.id.sigma.day4.response.InsertUserResponse;
import co.id.sigma.day4.response.ResponseBalance;
import co.id.sigma.day4.response.ResponseDeposit;
import co.id.sigma.day4.response.ResponseWithdrawl;
import co.id.sigma.day4.response.TransferFrom;
import id.co.sigma.day4.model.Users;
import id.co.sigma.day4.model.Account;
import id.co.sigma.day4.service.AccountService;
import id.co.sigma.day4.service.NasabahRegistrationService;

@RestController
public class NasabahRegistrationController {
	@Autowired
	private NasabahRegistrationService nasabahService;
	
	@Autowired
	private AccountService accountService;
	
	Set<Account> acc = new HashSet<>();


	@RequestMapping(path="/user/get/all")
	public List<Users> getAllNasabah() {
		return nasabahService.findAll();
	}
	
	@RequestMapping(path="/account/get/all")
	public List<Account> getAllAccount() {
		return accountService.findAllAccount();
	}

//	- inquiry data nasabah(tampilkan seluruh rekening yg dimiliki oleh nasabah dengan id yang di input di path)
//	path = /minicore/account/inquiry/{id}/all, 
//	response pasti berbentuk array json rekening

	@RequestMapping(path = "/account/inquiry/{id}/all")
	public List<Account> inquiryAll(@PathVariable("id") Long id) {
		Users nasabah = nasabahService.findByID(id);
		List<Account> rekening = new ArrayList<Account>();
		rekening.addAll(nasabah.getAccount());
		return rekening;
	}
	

//	@RequestMapping(value = "/account/insertUser**", method = { RequestMethod.POST })
//	public @ResponseBody InsertUserResponse insertUser(@RequestBody InsertUserRequest request) {
//
//		InsertUserResponse response = new InsertUserResponse();
//
//		Users user = new Users();
//		user.setId(request.getId_nasabah());
//		user.setNama_nasabah(request.getUserName());
//
//		Account accountInsert = accountService.findByAccountNumber(request.getAccountNumber());
//		acc.add(accountInsert);
//		
//		user.setNo_hp(request.getNo_hp());
//		user.setAlamat(request.getAlamat());
//		user.setAccount(acc);	
//
//		nasabahService.update(user);
//		response.setUserId(user.getId().toString());
//		response.setResponseCode("00");
//		response.setResponseMessage("SUCCESS");
//
//		return response;
//	}
//
//	@RequestMapping(value = "/account/updateUser**", method = { RequestMethod.POST })
//	public @ResponseBody InsertUserResponse updateUser(@RequestBody UpdateUserRequest request) {
//		InsertUserResponse response = new InsertUserResponse();
//		Users user = new Users();
//		user.setNama_nasabah(request.getNama_nasabah());
//
//		Account accountUpdate = accountService.findByAccountNumber(request.getAccountNumber());
//		acc.add(accountUpdate);
//		
//		user.setNo_hp(request.getNo_hp());
//		user.setAlamat(request.getAlamat());
//		user.setAccount(acc);
//
//		nasabahService.update(user);
//		response.setUserId(user.getId().toString());
//		response.setResponseCode("00");
//		response.setResponseMessage("SUCCESS");
//
//		return response;
//	}
	
	@RequestMapping(path = "/account/add")
	public BaseResponse add(@RequestBody Users user) {
		BaseResponse response = new BaseResponse();
		nasabahService.save(user);
		
		response.setResponseCode("00");
		response.setResponseMessage("SUCCESS");

		return response;
	}


//	// exception masih blm OK di postmannya
//	@RequestMapping(path = "/account/update")
//	@ExceptionHandler(value = { Exception.class })
//	public ResponseEntity<Object> update(@RequestBody Nasabah nasabah) {
//		Nasabah pemilik = nasabahService.findByID(nasabah.getId());
//		try {
//			if (nasabah.getNama_nasabah() == null || pemilik.getNama_nasabah() == nasabah.getNama_nasabah()) {
//				nasabah.setNama_nasabah(pemilik.getNama_nasabah());
//			} else if (nasabah.getNo_hp() == null || pemilik.getNo_hp() == nasabah.getNo_hp()) {
//				nasabah.setNo_hp(pemilik.getNo_hp());
//			} else if (nasabah.getAlamat() == null || pemilik.getAlamat() == nasabah.getAlamat()) {
//				nasabah.setAlamat(pemilik.getAlamat());
//			} else {
//				Error eError = new Error(HttpStatus.INTERNAL_SERVER_ERROR, "FAILED", " ");
//				return new ResponseEntity<Object>(eError.getMessage(), new HttpHeaders(), eError.getStatus());
//			}
//			nasabahService.save(nasabah);
//			Error eError = new Error(HttpStatus.ACCEPTED, "SUCCESS", " ");
//			return new ResponseEntity<Object>(eError.getMessage(), new HttpHeaders(), eError.getStatus());
//		} catch (NullPointerException ne) {
//			Error eError = new Error(HttpStatus.INTERNAL_SERVER_ERROR, "ERROR", ne.getLocalizedMessage());
//			return new ResponseEntity<Object>(eError.getMessage(), new HttpHeaders(), eError.getStatus());
//		}
//	}


	// Untuk Rekening

//	- cek saldo						
//	path = /minicore/account/balance 	
//	req : {"account" : "89911100"}, 
//	res : {"balance" : "50000"}

	@RequestMapping(path = "/account/balance")
	public @ResponseBody ResponseBalance cekSaldo(@RequestBody RequestBalance request) {
		Account accountBalance = accountService.findByAccountNumber(request.getNoRekening());
		ResponseBalance response = new ResponseBalance();
		response.setSaldo(accountBalance.getSaldo().toString());
		return response;
	}

//	- setor tunai
//	path = /minicore/transaction/deposit	
//	req = { "rekening" : "89xxxxx", "amount" : "1000000" }, 
//	res = { "rekening" : "89xxxxx", "saldo" : "500000000" }

	@RequestMapping(path = "/transaction/deposit")
	public ResponseDeposit setorTunai(@RequestBody RequestDeposit request) {
		Account accountDeposit = accountService.findByAccountNumber(request.getNoRekening());
		BigDecimal newBalance = accountDeposit.getSaldo();
		accountDeposit.setSaldo(newBalance.add(new BigDecimal(request.getAmount())));
		accountService.update(accountDeposit);
		ResponseDeposit response = new ResponseDeposit();
		response.setAmount(newBalance.toString());
		response.setNoRekening(accountDeposit.getNo_rekening());
		return response;
	}

//	- tarik tunai				
//	path = /minicore/transaction/withdrawal	
//	req = { "rekening" : "89xxxxx", "amount" : "1000000" }, 
//	res = { "rekening" : "89xxxxx", "saldo" : "400000000" }

	@RequestMapping(path = "/transaction/withdrawl")
	public ResponseWithdrawl withDrawl(@RequestBody RequestWithdrawl request) {
		Account accountWithdrawl = accountService.findByAccountNumber(request.getNoRekening());
		BigDecimal newBalance = accountWithdrawl.getSaldo();
		accountWithdrawl.setSaldo(newBalance.subtract(new BigDecimal(request.getAmount())));
		accountService.update(accountWithdrawl);
		ResponseWithdrawl response = new ResponseWithdrawl();
		response.setAmount(newBalance.toString());
		response.setNoRekening(accountWithdrawl.getNo_rekening());
		return response;
	}

//	@Transactional
//	- transfer antar nasabah		
//	path = /minicore/transaction/transfer	
//	req = { "rekeningAsal" : "89xxxxx", "rekeningTujuan" : "99xxxxx", "amount" : "1000000" }, 
//	res = {"message" : "SUCCESS/FAILED/ERROR"}

	// exception masih blm OK di postmannya
	@Transactional
	@RequestMapping(path = "/transaction/transfer")
	public BaseResponse transfer(@RequestBody TransferFrom request) {
		BaseResponse response = new BaseResponse();	
		try {

			Account rekeningAsal = accountService.findByAccountNumber(request.getFromNoRekening());
			Account rekeningTujuan = accountService.findByAccountNumber(request.getToNoRekening());

			BigDecimal saldoAsal = rekeningAsal.getSaldo();
			BigDecimal saldoTujuan = rekeningTujuan.getSaldo();
			if (saldoAsal.compareTo(saldoTujuan) >= saldoTujuan.longValue()) {
				rekeningAsal.setSaldo(saldoAsal.subtract(new BigDecimal(request.getAmount())));
				rekeningTujuan.setSaldo(saldoTujuan.add(new BigDecimal(request.getAmount())));

				accountService.update(rekeningAsal);
				accountService.update(rekeningTujuan);
				response.setResponseCode("00");
				response.setResponseMessage("SUCCESS");
				
			} else {
				response.setResponseCode("01");
				response.setResponseMessage("FAILED");
			}
			
			return response;
			
		} catch (NullPointerException ne) {
			response.setResponseCode("02");
			response.setResponseMessage("ERROR");
			
			return response;
		}

	}

}
