package id.co.sigma.day4.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import co.id.sigma.day4.request.InsertUserRequest;
import co.id.sigma.day4.request.RequestBalance;
import co.id.sigma.day4.request.RequestDeposit;
import co.id.sigma.day4.request.RequestWithdrawl;
import co.id.sigma.day4.request.UpdateUserRequest;
import co.id.sigma.day4.response.BaseResponse;
import co.id.sigma.day4.response.InsertUserResponse;
import co.id.sigma.day4.response.ResponseBalance;
import co.id.sigma.day4.response.ResponseDeposit;
import co.id.sigma.day4.response.ResponseWithdrawl;
import co.id.sigma.day4.response.TransferFrom;
import id.co.sigma.day4.feign.RekeningInterface;

//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;

import id.co.sigma.day4.model.Rekening;
//import id.co.sigma.day4.repository.RekeningRepository;

@Service
public class RekeningService {
	@Autowired
	private RekeningInterface rekeningRepository;

	public ResponseBalance cekSaldo(RequestBalance request) {
		return rekeningRepository.cekSaldo(request);
	}
	
	public ResponseDeposit setorTunai (RequestDeposit request) {
		return rekeningRepository.setorTunai(request);
	}
	
	public ResponseWithdrawl withDrawl( RequestWithdrawl request) {
		return rekeningRepository.withDrawl(request);
	}
	
	public BaseResponse transfer(TransferFrom request) {
		return rekeningRepository.transfer(request);
	}
//
//	public List<Rekening> findAll() {
//		return rekeningRepository.findAll();
//	}
//
//	public Rekening findByID(Long id) {
//		return rekeningRepository.findById(id).get();
//	}
//
//	public void save(Rekening rekening) {
//		rekeningRepository.save(rekening);
//	}
//
//	public void deleteByID(Long id) {
//		Rekening rekening = rekeningRepository.findById(id).get();
//		rekeningRepository.delete(rekening);
//	}
//
//	public Rekening findByNoRekening(String noRekening) {
//		return rekeningRepository.findByNoRekening(noRekening);
//	}
}
