package id.co.sigma.day4.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import co.id.sigma.day4.request.InsertUserRequest;
import co.id.sigma.day4.request.RequestBalance;
import co.id.sigma.day4.request.RequestDeposit;
import co.id.sigma.day4.request.RequestWithdrawl;
import co.id.sigma.day4.request.UpdateUserRequest;
import co.id.sigma.day4.response.BaseResponse;
import co.id.sigma.day4.response.InsertUserResponse;
import co.id.sigma.day4.response.ResponseBalance;
import co.id.sigma.day4.response.ResponseDeposit;
import co.id.sigma.day4.response.ResponseWithdrawl;
import co.id.sigma.day4.response.TransferFrom;
import id.co.sigma.day4.service.NasabahService;
import id.co.sigma.day4.service.RekeningService;

@RestController
public class Controller {
	@Autowired
	private RekeningService rekeningService;

	// Untuk Nasabah

	@RequestMapping(path = "/account/balance")
	public @ResponseBody ResponseBalance cekSaldo(@RequestBody RequestBalance request) {
		return rekeningService.cekSaldo(request);
	}
	
	@RequestMapping(path = "/transaction/deposit")
	public ResponseDeposit setorTunai(@RequestBody RequestDeposit request) {
		return rekeningService.setorTunai(request);
	}
	
	@RequestMapping(path = "/transaction/withdrawl")
	public ResponseWithdrawl withDrawl(@RequestBody RequestWithdrawl request) {
		return rekeningService.withDrawl(request);
	}
	
	@RequestMapping(path = "/transaction/transfer")
	public BaseResponse transfer(@RequestBody TransferFrom request) {
		return rekeningService.transfer(request);
}

}
