package id.co.sigma.day4.feign;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import co.id.sigma.day4.request.InsertUserRequest;
import co.id.sigma.day4.request.RequestBalance;
import co.id.sigma.day4.request.RequestDeposit;
import co.id.sigma.day4.request.RequestWithdrawl;
import co.id.sigma.day4.request.UpdateUserRequest;
import co.id.sigma.day4.response.BaseResponse;
import co.id.sigma.day4.response.InsertUserResponse;
import co.id.sigma.day4.response.ResponseBalance;
import co.id.sigma.day4.response.ResponseDeposit;
import co.id.sigma.day4.response.ResponseWithdrawl;
import co.id.sigma.day4.response.TransferFrom;
import id.co.sigma.day4.model.Nasabah;
import id.co.sigma.day4.model.Rekening;

@FeignClient("NasabahRegistration")
public interface RekeningInterface {
	
	@RequestMapping(path = "/nasabah/account/balance")
	public @ResponseBody ResponseBalance cekSaldo(@RequestBody RequestBalance request);
	
	@RequestMapping(path = "/nasabah/transaction/deposit")
	public ResponseDeposit setorTunai(@RequestBody RequestDeposit request);
	
	@RequestMapping(path = "/nasabah/transaction/withdrawl")
	public ResponseWithdrawl withDrawl(@RequestBody RequestWithdrawl request);
	
	@RequestMapping(path = "/nasabah/transaction/transfer")
	public BaseResponse transfer(@RequestBody TransferFrom request);
}
