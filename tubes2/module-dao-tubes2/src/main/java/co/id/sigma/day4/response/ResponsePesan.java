package co.id.sigma.day4.response;

import java.util.Set;

public class ResponsePesan {
	Long id;
	String nama_nasabah; 
	String no_hp; 
	String alamat;
	public ResponsePesan() {
		super();
	}

	public ResponsePesan(Long id, String nama_nasabah, String no_hp, String alamat) {
		this.id = id;
		this.nama_nasabah = nama_nasabah;
		this.no_hp = no_hp;
		this.alamat = alamat;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNama_nasabah() {
		return nama_nasabah;
	}

	public void setNama_nasabah(String nama_nasabah) {
		this.nama_nasabah = nama_nasabah;
	}

	public String getNo_hp() {
		return no_hp;
	}

	public void setNo_hp(String no_hp) {
		this.no_hp = no_hp;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

}
