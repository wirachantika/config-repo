package co.id.sigma.day4.response;

public class InsertUserResponse extends BaseResponse {

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
