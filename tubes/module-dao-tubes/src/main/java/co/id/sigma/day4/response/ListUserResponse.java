package co.id.sigma.day4.response;

public class ListUserResponse {

	private String nama_nasabah;
	private String no_hp;
	private String alamat;
	private String accountNumber;
	
	public ListUserResponse(String nama_nasabah, String no_hp, String alamat, String accountNumber) {
		super();
		this.nama_nasabah = nama_nasabah;
		this.no_hp = no_hp;
		this.alamat = alamat;
		this.accountNumber = accountNumber;
	}
	public String getNama_nasabah() {
		return nama_nasabah;
	}
	public void setNama_nasabah(String nama_nasabah) {
		this.nama_nasabah = nama_nasabah;
	}
	public String getNo_hp() {
		return no_hp;
	}
	public void setNo_hp(String no_hp) {
		this.no_hp = no_hp;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
}
