package co.id.sigma.day4.request;


public class InsertUserRequest {

	private String userName;
	private String no_hp;
	private String alamat;
	private String accountNumber;
	
	public InsertUserRequest(String nama_nasabah, String no_hp, String alamat, String accountNumber) {
		super();
		this.userName = nama_nasabah;
		this.no_hp = no_hp;
		this.alamat = alamat;
		this.accountNumber = accountNumber;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getNo_hp() {
		return no_hp;
	}
	public void setNo_hp(String no_hp) {
		this.no_hp = no_hp;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	
}
