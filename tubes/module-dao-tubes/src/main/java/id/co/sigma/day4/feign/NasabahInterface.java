package id.co.sigma.day4.feign;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import co.id.sigma.day4.request.InsertUserRequest;
import co.id.sigma.day4.request.UpdateUserRequest;
import co.id.sigma.day4.response.BaseResponse;
import co.id.sigma.day4.response.InsertUserResponse;
import id.co.sigma.day4.model.Nasabah;
import id.co.sigma.day4.model.Rekening;

@FeignClient("NasabahRegistration")
public interface NasabahInterface {
	
	@RequestMapping(path = "/nasabah/account/add")
	public BaseResponse add(@RequestBody Nasabah user);
	
	@RequestMapping(path = "/nasabah/account/inquiry/{id}/all")
	public List<Rekening> inquiryAll(@PathVariable("id") Long id);

//	@RequestMapping(value = "/account/updateUser**", method = { RequestMethod.POST })
//	@ResponseBody InsertUserResponse updateUser(@RequestBody UpdateUserRequest request);
}
