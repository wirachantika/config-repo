package id.co.sigma.day4.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import co.id.sigma.day4.request.InsertUserRequest;
import co.id.sigma.day4.request.UpdateUserRequest;
import co.id.sigma.day4.response.BaseResponse;
import co.id.sigma.day4.response.InsertUserResponse;
import id.co.sigma.day4.model.Nasabah;
import id.co.sigma.day4.model.Rekening;
import id.co.sigma.day4.service.NasabahService;

@RestController
public class Controller {
	@Autowired
	private NasabahService nasabahService;

	// Untuk Nasabah

	@RequestMapping(path = "/account/add")
	public BaseResponse add(@RequestBody Nasabah user) {
		return nasabahService.add(user);
	}
	
	@RequestMapping(path = "/account/inquiry/all/{id}")
	public List<Rekening> inquiryAll(@PathVariable("id") Long id_rekening){
		return nasabahService.inquiryAll(id_rekening);
	}

//	@RequestMapping(value = "/account/updateUser**", method = { RequestMethod.POST })
//	public @ResponseBody InsertUserResponse updateUser(@RequestBody UpdateUserRequest request) {
//		return nasabahService.updateNasabah(request);
//	}

}
